build:
	docker build -t blog .

bash:
	docker run -it -p 5000:5000 --rm -v $(shell pwd):/home/blog --name blog blog bash

interactive:
	docker run -it -p 5000:5000 --rm -v $(shell pwd):/home/blog --name blog blog sh /home/blog/run_server.sh

server:
	docker run -itd -p 5000:5000 --rm -v $(shell pwd):/home/blog --name blog blog sh /home/blog/run_server.sh
