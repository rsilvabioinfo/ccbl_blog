FROM continuumio/miniconda:latest
MAINTAINER Ricardo R. da Silva <ridasilva@usp.br>

ENV INSTALL_PATH /home/blog
RUN mkdir -p $INSTALL_PATH
WORKDIR $INSTALL_PATH

COPY environment.yml environment.yml
RUN conda env create -f environment.yml
RUN echo "source activate blog" > ~/.bashrc
ENV PATH /opt/conda/envs/blog/bin:$PATH


COPY . /home/blog 

ENV FLASK_APP app.py 

EXPOSE 2222 
CMD sh /home/blog/run_server.sh 
